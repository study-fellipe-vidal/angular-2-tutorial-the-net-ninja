FROM fellipevidal/node-env

RUN yarn global add @angular/cli --ignore-engines

WORKDIR /root/app

CMD ["zsh"]