import { Component, OnInit } from '@angular/core'
import { DataService } from '../data.service'
import { LoggingService } from '../logging.service'

@Component({
  selector: 'app-directory',
  templateUrl: './directory.component.html',
  styleUrls: ['./directory.component.css']
})
export class DirectoryComponent implements OnInit {
  ninjas?: any[]
  term: string | undefined = undefined

  constructor(private logger: LoggingService, private data: DataService) {}

  ngOnInit(): void {
    this.ninjas = this.data.fetchData()
    console.log(this.ninjas)
  }

  remove(e: any) {}
}
