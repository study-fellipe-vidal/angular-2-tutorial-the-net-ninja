import * as ninjas from '../assets/ninjas.json'
import { Injectable } from '@angular/core'

@Injectable({
  providedIn: 'root'
})
export class DataService {
  constructor() {}

  fetchData() {
    return JSON.parse(JSON.stringify(ninjas)).default
  }
}
